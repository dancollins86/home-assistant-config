# Automations
- House state- used to put the house into different modes throughout the day(morning and bed times can be overridden by input_datetime in UI)
- Wakeup - Sets lights in the morning(time can be overriden and disabled at weekends)
- Night Routine - Switches off lights at bedtime(time can be overriden and disabled at weekends)
- Outside Light - Switches on at civil twighlight and off at midnight
- Motion Sensors - Activates landing nightlight
- Vacation Mode - Activates lights at random times when input boolean activated
- Home/Away - Controls lighting when leaving/entering home zone
- TV Time - sets lighting at 8pm or when input boolean enabled

# Inputs

| Name | Type | Purpose |
| ------ | ------ | ----- |
| tv_time | boolean| Triggers TV Time Automation/Scene |
| vacation_mode | boolean | Activates automation for random lighting in the evening |
| ben_mode | boolean | Not in use yet, will be used to alter lighting if Ben is left in the house at night |
| guest_mode | boolean | Not in use yet, will be used to override stuff |
| wakeup_enabled | boolean | Used to toggle morning routine automation |
| wakeup_weekend | boolean | Used to disable morning routine at weekends |
| wakeup_time | time | Used to override time of wakeup automation(useful at weekends) |
| bobos_enabled | boolean | Used to toggle bed time routine automation|
| bobos_weekend | boolean | Used to disabled bed time automation at weekends|
| bobos_time | time | Used to override time of bed time automation(useful at weekends or when tired in the week) |

# Custom Components
- [https://github.com/claytonjn/hass-circadian_lighting](Circadian Lighting)
- [https://github.com/rogro82/hass-variables](Variables)


# Inspiration

- https://github.com/frenck/home-assistant-config
- https://github.com/allanak/homeassistant-config (Home State Variable)
- https://github.com/TribuneX (TV Source Selection)
